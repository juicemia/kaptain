## Running Minikube on Ubuntu

```
$ # install kubectl for ubuntu
$ # install minikube for ubuntu
$ sudo minikube start --vm-driver=none
```

## Volumes

Storage in Kubernetes.

`hostpath` type basically maps a volume to a path on the container host. Useful
for local testing.

## GlusterFS

This looks like it could function as a filesystem for a Kubernetes kernel.

Playing with it by following the guide [here](https://docs.gluster.org/en/latest/Quick-Start-Guide/Quickstart/).

Using Vagrant with Virtualbox to spin up the Fedora VMs.

To partition the secondary disk, I'm just using fdisk, pressing "n" and keeping
all the defaults.