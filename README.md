# kaptain

Kernel API on top of Kubernetes

## Why make a kernel out of Kubernetes?

A long time ago, computers were disparate subsystems of hardware networked together via pieces of condusive metal so they could work together. Programmers were left with the task of making it all work together in order for their programs to be able to use them. Eventually, people started writing shared libraries of code that would handle all the details for programers so that they could stick to writing what they wanted to: things that help people.

Today, computers are disparate subsystems of (virtual) hardware networked together via pieces of condusive metal (inside or outside of their physical cases) so they can work together. Programmers have been left with the task of making it all work together (Chef, Ansible, Terraform, Mesos, etc.) in order for their programs to be able to use them. Eventually, people started writing shared libraries of code (Kubernetes, Swarm) that would handle all the details for programmers so that they could stick to writing what they wanted to: things that help people.

Kubernetes is that kernel. It's just missing a userspace.

```
+-------------------------------+
|                               |
|           Kaptain             |
|                               |
+-------------------------------+ Userspace for interacting with kernel
|                               |
|          Kubernetes           |
|                               |
+-------------------------------+ Kernel for managing sub-systems
|       ||             ||       |
|  AWS  ||  OpenStack  ||  GCP  |
|       ||             ||       |
+-------------------------------+ Sub-systems for processes/memory (compute),
                                  I/O (Networking) and Storage (Volumes)
```
